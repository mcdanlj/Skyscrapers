# Skyscrapers Puzzle Game Pieces

This is a set of pieces for playing the "Skyscrapers" game of
spatial reasoning, also sometimes called "Towers".

## Rules of Play

The rules somewhat resemble sudoku. Place *n²* pieces, *n* each of
of *n* different heights, from 1 unit to *n* units, on an *n* x *n*
grid, following a few simple rules:

 * Each row contains only one piece of each height.
 * Each column contains only one piece of each height.
 * A number **in** a square means a piece of that unit height must be
   placed there.
 * A number **next to** the grid means that you must be able to "see"
   only that number of pieces from that location outside the grid; that is,
   that exactly that number of pieces must be placed in ascending order
   by height away from that edge in the adjacent row or column, and
   that no taller pieces are placed further away from the number.
   Note that the visible pieces might not be immediately adjacent;
   there may be shorter pieces *hidden* in between.

A proper Skyscrapers puzzle has a unique solution.

In general, like sudoku, simple Skyscraper puzzles have many of
these placement constraints expressed, such that they are redundant.
In simple games, the number of "visible" towers might be specified
at every station along each row or columns. More challenging puzzles
have fewer redundant constraints.

Most published printable grids use a 1" (25.4mm) spacing, which are
often played with 3/4" (19.05mm) locking unifix "cubes". This leaves
some room for manipulating the pieces while solving the puzzle.
This set uses 20mm unit size, which is also playable on those common
grids.

You can also [try this puzzle
online](https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/towers.html).

## Printing

The *n*Unit blocks, which are provided for one to six units in
height, are printable without supports.

 * If you print the STEP files, they are hollow inside and are
   intended to be self-supporting using a "cascading diamond"
   internal feature that works with layer thickness 0.3mm or thinner.
   **No generated infill is required.**

 * If you print the STL files, be aware that due to a characteristic
   of how FreeCAD exports the STLs, the slicer may not recognize
   the internal void. In that case, you may need either to enable
   some infill (if you have bridging failures supporting the top
   of the block), or in PrusaSlicer and its derivatives, there is
   a Print Settings → Advanced → Slicing → Slicing Mode that
   can be changed from "Regular" to "Even-odd", which will enable
   it to print the internal voids correctly.

The game will be easier to play if each size is printed in its own
color. All the 1Unit blocks in one color, all the 2Unit in another
color, and so forth.

For any particular grid size, you will need to print at least that
many pieces of each size. To do a 4x4 grid, you will need 4 1Unit,
4 2Unit, 4 3Unit, and 4 4Unit pieces. To do a 5x5 grid, you will
need 5 1Unit, 5 2Unit, 5 3Unit, 5 4Unit, and 5 5Unit pieces.

## Getting Started

Included are some sample puzzles to get started.

From https://buildingmathematicians.wordpress.com/2017/03/14/skyscraper-templates/ (Mark Chubb)

 * `Skyscrapers_-_4_by_4_-_beginner.pdf`
 * `Skyscrapers_-_4_by_4_-_advanced.pdf`
 * `Skyscrapers_-_5_by_5_-_beginner.pdf`
 * `Skyscrapers_-_5_by_5_-_advanced.pdf`
 * `Skyscrapers-collection.pdf`

From https://jrmf.org/puzzle/skyscrapers/ (Julia Robinson Math Festival)
 * `Skyscrapers-Festival-Guide.pdf`

## Customization

The [FreeCAD](https://freecad.org/) source for these blocks is
included. There is a spreadsheet called `p` (for parameters)
that contains parameters you can change, if you want.

 * The `side` parameter allows you to choose a unit size; the
   default is 20mm

 * The `thickness` parameter sets the wall thickness where there
   is no chamfer; the default is 1.5mm

 * The `chamfer` parameter sets the normal depth of the chamfer;
   the default is 1mm, and it must be smaller than the thickness
   by at least one reasonable filament line width.

 * The `clearance` parameter sets the distance between the different
   height models for viewing purposes.

## License

The models are Copyright Michael K Johnson, and may be used freely
under the [CC-BY](https://creativecommons.org/licenses/by/4.0/)
license.

The PDFs have no expressed license terms but were published for general
use by their respective authors, and are included here for convenience.
